<html lang="en">

	  <head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Aplikasi Inventaris Sarana dan Prasarana</title>

		<!-- Bootstrap core CSS-->
		<link href="admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom fonts for this template-->
		<link href="admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
		<!-- Page level plugin CSS-->
		<!-- Custom styles for this template-->
		<link href="admin/css/sb-admin.css" rel="stylesheet">	
		 <link href="admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
	  </head>
	  <style>
	.bg-dark {
    background-color: #696969 !important;
}
	</style>
	  <style>
	   body{
		  background-image:url(admin/gambar/bg2.png);
		  background-size:cover;
		  background-attachment:fixed;
		  text-align:justify;
	  }p{
		  color:black;
		  justify-content;
	  }
	  </style>
	<?php
	 function anti ($data)
	 {
		   $filter = mysql_real_escape_string(stripcslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		   return $filter;
	 }
	 ?>
	  <body id="page-top">
		<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
		  <a class="navbar-brand mr-1" href="index.php">Inventaris Sarana dan Prasarana SMK</a>
		  <!-- Navbar Search -->
		  <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
		  </form>
		  <!-- Navbar -->
		  <ul class="navbar-nav ml-auto ml-md-0">
			
	&nbsp;
	&nbsp;
	&nbsp;
	&nbsp;
			<li class="nav-item dropdown no-arrow mx-1">
			 <a class="navbar-brand mr-1" href="index.php">Dashboard</a>
			</li>			
		  </ul>
		</nav>
      <div id="content-wrapper">
        <div class="container-fluid">
			 <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login</div>
        <div class="card-body">
         <form action="cek_login.php" method="post">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="Username" autocomplete="off" name="username" type="text" pattern="[A-Za-z]{8,0}" autofocus="" maxlength="22" required="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" autocomplete="off"  name="password" type="password" value="" maxlength="22" required="">
							</div>
							
							<button type="submit" class="btn btn-lg btn-dark btn-block">Login</a></fieldset>
					</form>
           <div class="text-center">
							<a class="d-block small mt-3" href="forgot.php">Lupa Password</a>
					</div>
        </div>
      </div>
    </div> 
        </div>
      </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
 
  </body>

</html>
