-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 07 Apr 2019 pada 07.17
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `deskripsi`
--

CREATE TABLE `deskripsi` (
  `id` int(11) NOT NULL,
  `kata` text NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `deskripsi`
--

INSERT INTO `deskripsi` (`id`, `kata`, `foto`) VALUES
(1, '<p>Inventarisasi berasal dari kata &ldquo;inventaris&rdquo; (Latin = inventarium) yang berarti daftar barang-barang, bahan, dan sebagainya. Inventarisasi sarana dan prasarana SMK adalah pencatatan atau pendaftaran barang-barang milik sekolah ke dalam suatu daftar inventaris barang secara tertib dan teratur menurut ketentuan dan tata cara yang berlaku.</p>\r\n\r\n<p><br />\r\n<span style="color:null">Daftar inventarisasi barang yang disusun dalam suatu organisasi yang lengkap, teratur dan berkelanjutan dapat memberikan manfaat, yakni sebagai berikut:</span></p>\r\n\r\n<ol>\r\n	<li><span style="color:null">&nbsp;Menyediakan data dan informasi dalam rangka menentukan kebutuhan dan menyusun rencana kenutuhan barang.</span></li>\r\n	<li><span style="color:null">&nbsp;Memberikan data dan informasi untuk dijadikan bahan/pedoman dalam pengarahan pengadaan barang.</span></li>\r\n	<li><span style="color:null">&nbsp;Memberikan data dan informasi untuk dijadikan bahan/pedoman dalam penyaluran barang.</span></li>\r\n	<li><span style="color:null">Memberikan data dan informasi dalam menentukan keadaan barang (tua, rusak, lebih) sebagai dasar untuk menetapkan pengnhapusannya.</span></li>\r\n</ol>\r\n\r\n<p><span style="color:null">&nbsp;Disini juga&nbsp;ada pembagian level yang bisa mengakses &nbsp;ini terdiri dari :</span></p>\r\n\r\n<p>&nbsp;</p>\r\n', '300320190359381.jpg'),
(2, '<p>Di dalam Admin terdapat fitur :</p>\r\n\r\n<p>Login</p>\r\n\r\n<p>Logout</p>\r\n\r\n<p>Inventaris</p>\r\n\r\n<p>Peminjaman</p>\r\n\r\n<p>Pengembalian</p>\r\n\r\n<p>Laporan</p>\r\n\r\n<p>Dll</p>\r\n', '3003201903575321f4409f21392894c29470be7c53cb10_test-real-user-experience-the-free-clipart-computer-user_619-500-1.png'),
(3, '<p>Di dalam Peminjaman terdapat fitur :</p>\r\n\r\n<p>Login</p>\r\n\r\n<p>Logout</p>\r\n\r\n<p>Peminjaman</p>\r\n', '30032019040058005-get-money-02.png'),
(4, '<p>Inventaris</p>\r\n', '30032019040130inventory-icon-6.png'),
(5, '<p>Di dalam Operator terdapat fitur :</p>\r\n\r\n<p>Login</p>\r\n\r\n<p>Logout</p>\r\n\r\n<p>Peminjaman</p>\r\n\r\n<p>Pengembalian</p>\r\n', '30032019040201Telephone-FAQ.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id` int(11) NOT NULL,
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(5) NOT NULL,
  `status` enum('Y','N') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id`, `id_detail_pinjam`, `id_inventaris`, `jumlah_pinjam`, `status`) VALUES
(12, 7, 6, 5, 'N'),
(13, 8, 1, 5, 'N'),
(14, 8, 4, 5, 'N'),
(15, 9, 1, 5, 'N'),
(16, 9, 2, 5, 'N'),
(17, 10, 1, 1, 'N'),
(18, 10, 2, 5, 'N'),
(19, 11, 1, 1, 'Y'),
(20, 11, 2, 1, 'Y');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(22) NOT NULL,
  `kondisi` varchar(22) NOT NULL,
  `keterangan_inv` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(22) NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `keterangan_inv`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`) VALUES
(1, 'Laptop', 'baik', 'pemasukan', 105, 1, '2019-01-02', 1, 'INV00001', 1),
(2, 'HeadSet', 'baik', 'pemasukan', 101, 3, '2019-01-02', 1, 'INV00002', 2),
(4, 'Kursi', 'baik', 'pemasukan', 55, 3, '2019-01-24', 3, 'INV00004', 1),
(6, 'Proyektor', 'baik', 'pemasukan', 90, 3, '2018-12-13', 1, 'INV00005', 1),
(7, 'Meja', 'baik', 'pemasukan', 100, 1, '2018-10-11', 3, 'INV00007', 3),
(8, 'bola', 'baik', 'pemasukan', 30, 1, '2019-02-06', 3, 'INV00008', 1),
(10, 'bangku', 'baik', 'pemasukan', 100, 1, '2019-02-11', 1, 'INV00010', 1),
(11, 'Keyboard', 'baik', 'pemasukan', 30, 3, '2019-02-13', 3, 'INV00011', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(22) NOT NULL,
  `kode_jenis` varchar(22) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'Furniture ', 'JN0001', 'Bagus\r\n'),
(3, 'Elektronik', 'JN0002', 'Bagus');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id_level` int(3) NOT NULL,
  `nama_level` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'administrator'),
(2, 'operator'),
(3, 'peminjam');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(11) NOT NULL,
  `nama_pegawai` varchar(22) NOT NULL,
  `nip` varchar(17) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(1, 'Selgi Aulia', '32141241319987', 'Ciomas Kreteg'),
(2, 'Sani Lestari', '12019391232200', 'Ciomas Kampung Horta'),
(3, 'Muhammad Wahyudin', '32094359856423', 'Sbj'),
(4, 'Ira Yuniar', '32193359866886', 'Kp.Ciherang Sawah Kidul'),
(5, 'Muhammad  Farhan', '92094359776555', 'Kp.Ciomas Kreteg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `kd_pinjam` varchar(24) NOT NULL,
  `tanggal_pinjam` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `tanggal_kembali` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status_peminjaman` enum('Pinjam','Kembali','','') DEFAULT 'Pinjam',
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `kd_pinjam`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_pegawai`) VALUES
(7, 'PMN0007', '2019-04-02 03:45:37', '2019-04-02 05:39:56', 'Kembali', 1),
(8, 'PMN0008', '2019-04-04 01:57:25', '2019-04-04 02:21:04', 'Kembali', 1),
(9, 'PMN0009', '2019-04-04 02:21:38', '2019-04-04 02:22:01', 'Kembali', 1),
(10, 'PMN0010', '2019-04-05 03:16:57', '2019-04-05 14:45:40', 'Kembali', 1),
(11, 'PMN0011', '2019-04-05 15:06:54', '2019-04-05 15:06:54', 'Pinjam', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(22) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `nama_petugas` varchar(22) NOT NULL,
  `id_level` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `password`, `email`, `nama_petugas`, `id_level`, `id_pegawai`) VALUES
(1, 'admin', '641a144dc4369b642ab4aee4d534024c', 'nurulfitrir17@gmail.com', 'Resky', 1, 1),
(2, 'operator', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Aji', 2, 3),
(3, 'sani', 'b82ef4c44602c1146244738d9d768dcd', '', 'Sani', 3, 2),
(4, 'irayuniar', 'e0a9ab6526071b0dbee156a43cc2400d', 'reskysaepudin.23@gmail.com', 'Ira Yuniar', 3, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `recovery_keys`
--

CREATE TABLE `recovery_keys` (
  `userID` varchar(500) NOT NULL,
  `token` varchar(500) NOT NULL,
  `valid` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(22) NOT NULL,
  `kode_ruang` varchar(22) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(1, 'Lab Kom', 'RNG0001', 'Baik '),
(3, 'Perpustakaan', 'RNG0002', 'Baik'),
(4, 'Kelas ', 'RNG0003', 'baik'),
(5, 'Tata Usaha', 'RNG0005', 'baik'),
(6, 'Ruang Guru', 'RNG0004', 'baik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `smtdetail_pinjam`
--

CREATE TABLE `smtdetail_pinjam` (
  `kode_inventaris` varchar(22) NOT NULL,
  `id_detail_pinjam` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `jumlah_pinjam` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `deskripsi`
--
ALTER TABLE `deskripsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_inventaris` (`id_inventaris`),
  ADD KEY `id_detail_pinjam` (`id_detail_pinjam`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_petugas` (`id_petugas`),
  ADD KEY `id_ruang` (`id_ruang`),
  ADD KEY `id_jenis` (`id_jenis`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `id_level` (`id_level`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `recovery_keys`
--
ALTER TABLE `recovery_keys`
  ADD PRIMARY KEY (`userID`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- Indexes for table `smtdetail_pinjam`
--
ALTER TABLE `smtdetail_pinjam`
  ADD KEY `id_inventaris` (`id_inventaris`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `deskripsi`
--
ALTER TABLE `deskripsi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD CONSTRAINT `detail_pinjam_ibfk_1` FOREIGN KEY (`id_inventaris`) REFERENCES `inventaris` (`id_inventaris`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `inventaris`
--
ALTER TABLE `inventaris`
  ADD CONSTRAINT `inventaris_ibfk_1` FOREIGN KEY (`id_jenis`) REFERENCES `jenis` (`id_jenis`) ON UPDATE CASCADE,
  ADD CONSTRAINT `inventaris_ibfk_2` FOREIGN KEY (`id_ruang`) REFERENCES `ruang` (`id_ruang`) ON UPDATE CASCADE,
  ADD CONSTRAINT `inventaris_ibfk_3` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) ON UPDATE CASCADE,
  ADD CONSTRAINT `petugas_ibfk_2` FOREIGN KEY (`id_pegawai`) REFERENCES `pegawai` (`id_pegawai`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
