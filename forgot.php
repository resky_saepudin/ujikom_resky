<?php
include "koneksi.php";
?>
<?php
include ('function.php');
  
if(isset($_POST['submit']))
{
  $email = $_POST['email'];
  $email = mysqli_real_escape_string($koneksi, $email);
  
  if(checkUser($email) == "true")
  {
    $userID = UserID($email);
    $token = generateRandomString();
    
    $query = mysqli_query($koneksi, "INSERT INTO recovery_keys (userID, token) VALUES ($userID, '$token') ");
    if($query)
    {
       $send_mail = send_mail($email, $token);


      if($send_mail === 'success')
      {
         $msg = '<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold">Password berhasil di kirim ke email anda <a href="#" class="alert-link">this important</a> alert message.
                    </div>';
         $msgclass = 'bg-success';
      }else{
        $msg = '<div class="alert alert-danger alert-styled-left alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold">Email</span> Wong <a href="#" class="alert-link">try submitting again</a>.
                    </div>';
        $msgclass = 'bg-danger';
      }



    }else
    {
        $msg = '<div class="alert alert-danger alert-styled-left alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold">Oh snap!</span>Email Tidak Terdaftar.<a href="#" class="alert-link">try submitting again</a>.
                    </div>';
         $msgclass = 'bg-danger';
    }
    
  }else
  {
    $msg = '<div class="alert alert-danger alert-styled-left alert-bordered">
                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                    <span class="text-semibold">Email! </span> tidak terdaftar di Database <a href="#" class="alert-link">try submitting again</a>.
                    </div>';
         $msgclass = 'bg-danger';
  }
}

?>
<html lang="en">

	  <head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Aplikasi Inventaris Sarana dan Prasarana</title>

		<!-- Bootstrap core CSS-->
		<link href="admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom fonts for this template-->
		<link href="admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
		<!-- Page level plugin CSS-->
		<!-- Custom styles for this template-->
		<link href="admin/css/sb-admin.css" rel="stylesheet">	
		 <link href="admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
	  </head>
	  <style>
	.bg-dark {
    background-color: #696969 !important;
}
	</style>
	   <style>
	   body{
		  background-image:url(admin/gambar/bg2.png);
		  background-size:cover;
		  background-attachment:fixed;
		  text-align:justify;
	  }p{
		  color:black;
		  justify-content;
	  }
	  </style>
	<?php
	 function anti ($data)
	 {
		   $filter = mysql_real_escape_string(stripcslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		   return $filter;
	 }
	 ?>
	  <body id="page-top">
		<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
		  <a class="navbar-brand mr-1" href="index.php">Inventaris Sarana dan Prasarana SMK</a>
		  <!-- Navbar Search -->
		  <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
		  </form>
		  <!-- Navbar -->
		  <ul class="navbar-nav ml-auto ml-md-0">
			<a class="navbar-brand mr-1" href="#">
				
	</a>
	&nbsp;
	&nbsp;
	&nbsp;
	&nbsp;
			<li class="nav-item dropdown no-arrow mx-1">
			 <a class="navbar-brand mr-1" href="login.php">Login</a>
			</li>			
		  </ul>
		</nav>
      <div id="content-wrapper">
        <div class="container-fluid">
			 <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Lupa Password</div>
        <div class="card-body">
         <form action="" method="POST">
		 <?php if(isset($msg)) {?>
    <div><?php echo $msg; ?></div>
<?php } ?>
         <form action="#" method="post">
			<div class="form-group">
					<input class="form-control" placeholder="Masukan Email Anda" autocomplete="off"  name="email" type="email" value="" maxlength="45" required="">
			</div>
			 <button type="submit" name="submit" class="btn btn-dark btn-block">Reset Password</button>
		</form>
           <div class="text-center">
							<a class="d-block small mt-3" href="login.php">Login</a>
					</div>
        </div>
      </div>
    </div> 
        </div>
      </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
 
  </body>

</html>
