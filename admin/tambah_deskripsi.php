<?php
include 'header.php';
?>
<?php
include 'cek_level.php';
?>
    <div id="wrapper">

     <?php
	include 'menu.php';
	?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Deskripsi</h3>
          <hr>
			<center><h5>Tambah Deskripsi</h5></center>
			<hr>
          <form action="simpan_deskripsi.php" method="POST"  enctype="multipart/form-data"> 
				<div class="control-group">
							  <label class="control-label" for="fileInput">Tambah File Gambar</label>
							  <div class="controls">
								<input class="input-file uniform_on" id="Filegambar" name="foto" type="file">
							  </div>
							</div>
				<br>							
			<div class="form-group">
            <label>Deskripsi</label>
            <textarea id="text-ckeditor" name="kata" class="form-control" placeholder="Masukan Keterangan Gambar"></textarea>
			<script>CKEDITOR.replace('text-ckeditor')</script>
            </div>
			
							
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Simpan</button>
								<a href="deskripsi.php"><button type="button" class="btn btn-">Kembali</button>
							</div>
							<br>
							<br>
			</form>
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

<?php
include 'script.php';
?>

  </body>

</html>
