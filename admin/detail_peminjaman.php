<?php
include 'header.php';
?>
    <div id="wrapper">

      <!-- Sidebar -->
    <?php
	include 'menu.php';
	?>
      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Peminjaman</h3>
          <hr>
		   <div class="row">
								<div class="col-md-10"></div>
								<div class="col-md-2">
					<a class="btn btn-dark fa fa-reply" href="peminjaman.php">Kembali</a>
		  </div>
		  </div>
		  <br>
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Detail Peminjaman</div>
            <div class="card-body">
              <div class="table-responsive">
										<?php
										include "koneksi.php";
										$id_peminjaman=$_GET['id_peminjaman'];
										$select=mysqli_query($koneksi,"select * from peminjaman left join detail_pinjam on detail_pinjam.id_detail_pinjam=peminjaman.id_peminjaman
																					  left join inventaris on detail_pinjam.id_inventaris=inventaris.id_inventaris
																					  left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai
																						where id_peminjaman='$id_peminjaman' AND status='Y'");
										 $nomor=mysqli_fetch_array(mysqli_query($koneksi,"select * from peminjaman left join pegawai on pegawai.id_pegawai=peminjaman.id_pegawai where id_peminjaman='$id_peminjaman'"));
										?>
							<div class="row" style="margin-left: 660px">
								<div class="col-md-4">
								<label>Kode Pinjam</label><input type="text" class="form-control"  value="<?php echo $nomor['kd_pinjam'];?>" readonly="readonly">      
								</div>
								<div class="col-md-6">
                                <label>Tanggal Pinjam</label><input type="text" class="form-control"  value="<?php echo $nomor['tanggal_pinjam'];?>" readonly="readonly">   
                                </div> 
								</div>
								&nbsp;
                        <table class='table table-hover'>
                                <thead>
                                    <tr>
                                        <td>Kode Barang</td>
                                        <td>Nama Barang</td>
                                        <td>Jumlah Pinjam</td>
                                    </tr>
                                </thead>
								<?php
                                while($r=mysqli_fetch_array($select)){
								?>
                                   <tr>
                                            <td><?php echo $r['kode_inventaris']; ?></td>
                                            <td><?php echo $r['nama']; ?></td>
                                            <td><?php echo $r['jumlah_pinjam']; ?></td>
                                        </tr>
                                <?php } ?>
									<br>
									<tr>
                                        <td colspan='2'><h5 align='right'>Nama Pegawai</h5></td>
                                        <td colspan='2'><h5><?php echo $nomor['nama_pegawai'];?></h5></td>
                                    </tr>
                                    </table>
              </div>
            </div>
            
          </div>

        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

<?php
include 'script.php';
?>

  </body>

</html>
