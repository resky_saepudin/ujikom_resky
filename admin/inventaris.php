<?php
include 'header.php';
?>
<?php
include 'cek_level.php';
?>
    <div id="wrapper">

		<?php
		include 'menu.php';
		?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Inventaris</h3>
          <hr>
		  <a href="tambah_inventaris.php" class="btn btn-primary fa fa-plus" style="margin-left: 900px">Tambah Barang</a>
		  <br/>
		  <br>
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Inventaris</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
							<th>No</th>
							<th>Nama Barang</th>
							<th>Kondisi Barang</th>
							<th>Keterangan</th>
							<th>Jumlah Barang</th>
							<th>Nama Jenis</th>
							<th>Tanggal Register</th>
							<th>Nama Ruang</th>
							<th>Kode Inventaris</th>
							<th>Nama Petugas</th>
							<th>Aksi</th>
                    </tr>
                  </thead>
                  
				  <tbody>
                  <?php
							include "koneksi.php";
							$no=1;
							$select=mysqli_query($koneksi,"select * from inventaris left join jenis on inventaris.id_jenis=jenis.id_jenis
																					left join ruang on inventaris.id_ruang=ruang.id_ruang
																					left join petugas on inventaris.id_petugas=petugas.id_petugas");
							while($data=mysqli_fetch_array($select))
									{
							?>
										
										<tr class="succes">
											<td><?php echo $no++ ?></td>
											<td><?php echo $data['nama']; ?></td>
											<td><?php echo $data['kondisi']; ?></td>
											<td><?php echo $data['keterangan_inv']; ?></td>
											<td><?php echo $data['jumlah']; ?></td>
											<td><?php echo $data['nama_jenis']; ?></td>
											<td><?php echo $data['tanggal_register']; ?></td>
											<td><?php echo $data['nama_ruang']; ?></td>
											<td><?php echo $data['kode_inventaris']; ?></td>
											<td><?php echo $data['nama_petugas']; ?></td>
												<td>
													<a href="edit_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>"><button type="button" class="btn btn-outline btn-primary fa fa-edit"></button></a>
													<a href="hapus_inventaris.php?id_inventaris=<?php echo $data['id_inventaris']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash"></button></a> 
                                
												</td>
										</tr>
											<?php	
									}
											?>
					</tbody>
                </table>
              </div>
            </div>

          </div>
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

<?php
include 'script.php';
?>

  </body>

</html>
