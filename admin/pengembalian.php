<?php
include 'header.php';
?>
<?php
include 'cek_level2.php';
?>
    <div id="wrapper">

      <!-- Sidebar -->
    <?php
	include 'menu.php';
	?>
      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Pengembalian</h3>
          <hr>
		   <center><div class="panel-body">
						<div class="col-lg-5">
						<label>Pilih Kode Peminjaman</label>
							<form method="GET">
							<select name="id_peminjaman" required="Tidak ada Data Pinjam" class="form-control m-bot15">
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysqli_query($koneksi,"SELECT id_peminjaman,kd_pinjam from peminjaman where status_peminjaman='Pinjam' ");
								while($row = mysqli_fetch_assoc($result))
								{
								echo "<option value='$row[id_peminjaman]'>$row[kd_pinjam]</option>";
								} 
								?>
									</select>
									<br/>
								<button type="submit" name="pilih" class="btn btn-outline btn-primary">Tampilkan</button>
							</form></center>
							<br>
			              <?php
							if(isset($_GET['pilih'])){?>				
			<div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Data Pinjam</div>
								  <form action="hapus_pmj.php" method="post" role="form">
									<div class="table-responsive">
										<?php
										include "koneksi.php";
										$id_peminjaman=$_GET['id_peminjaman'];
										$select=mysqli_query($koneksi,"select * from peminjaman i left join detail_pinjam p on p.id_detail_pinjam=i.id_peminjaman
																						left join inventaris v on p.id_inventaris=v.id_inventaris
																						where id_peminjaman='$id_peminjaman' AND status ='Y'");
										$nomor=mysqli_fetch_array(mysqli_query($koneksi,"select * from peminjaman left join pegawai on pegawai.id_pegawai=peminjaman.id_pegawai where id_peminjaman='$id_peminjaman'"));
										?>
								<div class="row">
								<div class="col-md-6"></div>
								<div class="col-md-3">
                                <label>Kode Peminjam</label><input name="kd_pinjam" type="text" class="form-control"  value="<?php echo $nomor['kd_pinjam'];?>" readonly="readonly">   
                                </div> 
								<div class="col-md-3">
                                <label>Tanggal Peminjaman</label><input name="tanggal_pinjam" type="text" class="form-control"   value="<?php echo $nomor['tanggal_pinjam'];?>" readonly="readonly">   
                                </div> 
								</div>
								&nbsp;
                        <table class='table table-hover'>
                                <thead>
                                    <tr>
                                        <td>Nama Barang</td>
										<td>Jumlah Pinjam</td>
										<td>Pilih barang</td>
                                    </tr>
                                </thead>
								<?php
                                while($r=mysqli_fetch_array($select)){
								?>
                                   <tr>
                                           <td><input name="id_inventaris[]" type="text" class="form-control"  value="<?php echo $r['id_inventaris'];?>.<?php echo $r['nama'];?>" autocomplete="off" readonly="readonly">
												<input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $r['id_peminjaman'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
												<input name="id[]" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $r['id'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
												<input name="id_detail_pinjam[]" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $r['id_detail_pinjam'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly"></td>
                                            <td><input name="jumlah[]" id="jumlah" type="text" class="form-control" placeholder="" value="<?php echo $r['jumlah_pinjam'];?>" autocomplete="off" maxlength="11" readonly="readonly">										
												<input name="status_peminjaman" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $r['status_peminjaman'];?>" autocomplete="off" maxlength="11" required="">
												<input name="status[]" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $r['status'];?>" autocomplete="off" maxlength="11" required="">
												<input name="tanggal_kembali" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" autocomplete="off" maxlength="11" required="">
											</td>
											
										<td><input type="checkbox" name="kembali[]" required="Anda Belum Menceklis" value="<?php echo $r['id'];?>"></td>

                                        </tr>
                                <?php } ?>
									<br>
									<tr>
                                        <td colspan='2'><h5 align='right'>Nama Pegawai</h5></td>
                                        <td colspan='2'><h5><input name="id_pegawai" type="text" class="form-control"  value="<?php echo $nomor['id_pegawai'];?>.<?php echo $nomor['nama_pegawai'];?>" readonly></h5></td>
                                    </tr>
                                    </table>
              
								<button type="submit"  class="btn btn-success">Kembalikan</button>
								<br>
								<br>
								 </div>
                                 </form>
            </div>
          </div>
		  <?php } ?>

			<br>
			<hr>	
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
             Table Pengembalian</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
						<tr><th>No</th>
							<th>Kode Peminjaman</th>
							<th>Tanggal Pinjam</th>
							<th>Tanggal Kembali</th>
							<th>Status Peminjaman</th>
							<th>Nama Pegawai</th>
							<th>Aksi</th>
                    </tr>
                  </thead>
                  
				  <tbody>
                  <?php
							include "koneksi.php";
							$no=1;
							$select=mysqli_query($koneksi,"select * from peminjaman left join pegawai on pegawai.id_pegawai=peminjaman.id_pegawai where status_peminjaman='Kembali'");
							while($data=mysqli_fetch_array($select))
									{
							?>
										
										<tr class="succes">
											<td><?php echo $no++ ?></td>
											<td><?php echo $data['kd_pinjam']; ?></td>
											<td><?php echo $data['tanggal_pinjam']; ?></td>
											<td><?php echo $data['tanggal_kembali']; ?></td>
											<td><?php echo $data['status_peminjaman']; ?></td>
											<td><?php echo $data['nama_pegawai']; ?></td>
											
												<td>													
													<a href="hapus_pengembalian.php?id_peminjaman=<?php echo $data['id_peminjaman']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash"></button></a>
												</td>
										</tr>
											<?php	
									}
											?>
					</tbody>
                </table>
              </div>
            </div>
            
          </div>

        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

<?php
include 'script.php';
?>

  </body>

</html>
