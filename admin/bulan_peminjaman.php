<?php
include 'header.php';
?>
<?php
include 'cek_level.php';
?>
    <div id="wrapper">

		<?php
		include 'menu.php';
		?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Laporan Peminjaman</h3>
          <hr>
		  <div class="row">
								<div class="col-md-10"></div>
								<div class="col-md-2">
					<a class="btn btn-dark fa fa-reply" href="l_peminjaman.php">Kembali</a>
		  </div>
		  </div>
		  		   <center><div class="panel-body">
						<div class="col-lg-5">
						<label>Pilih Bulan</label>
							<form method="get">
							<select name="bulan" class="form-control m-bot15">
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysqli_query($koneksi,"SELECT MONTH (tanggal_pinjam) as bulan from peminjaman GROUP BY MONTH(tanggal_pinjam)");
								while($row = mysqli_fetch_assoc($result))
								{
								echo "<option value='$row[bulan]'>$row[bulan]</option>";
								} 
								?>
									</select>
								<br/>
							<button type="submit"  class="btn btn-outline btn-primary">Tampilkan</button>
			</form></center>
		  <br>	
		  <?php 	if(isset($_GET['bulan'])){ ?>
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Peminjaman</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
							<th>No</th>
							<th>Kode Pinjam</th>
							<th>Tanggal Pinjam</th>
							<th>Nama Pegawai</th>
							<th>Nama Barang</th>
							<th>Jumlah Pinjam</th>
                    </tr>
                  </thead>
                  
				  <tbody>
                  <?php
							include "koneksi.php";
							$no=1;
							$select=mysqli_query($koneksi,"select * from peminjaman left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai
																					left join detail_pinjam on detail_pinjam.id_detail_pinjam=peminjaman.id_peminjaman
																					left join inventaris on inventaris.id_inventaris=detail_pinjam.id_inventaris
																					where MONTH(tanggal_pinjam) = '$_GET[bulan]'");
							while($data=mysqli_fetch_array($select))
									{
							?>
										
										<tr class="succes">
											<td><?php echo $no++ ?></td>
											<td><?php echo $data['kd_pinjam']; ?></td>
											<td><?php echo $data['tanggal_pinjam']; ?></td>
											<td><?php echo $data['nama_pegawai']; ?></td>
											<td><?php echo $data['nama']; ?></td>
											<td><?php echo $data['jumlah_pinjam']; ?></td>
										</tr>
											<?php	
									}
											?>
					</tbody>
                </table>
              </div>
            </div>
          </div>
		  <form method="POST" action="export2_peminjaman.php" style="display: inline-block;">
				<input type="hidden" name="bulan" value=<?php echo $_GET['bulan']?>>
				<button type="submit" class="btn btn-success fa fa-print">Export to Excel</button>
			</form>
		 <form method="POST" action="pdf2_peminjaman.php" style=" display: inline;">
			 <input type="hidden" name="bulan" value=<?php echo $_GET['bulan']?>>
			<button type="submit" class="btn btn-danger fa fa-print">Print PDF</button>
			</form>
					<br>
					<br>
		  <?php } ?>
        </div>
			</div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

<?php
include 'script.php';
?>

  </body>

</html>
