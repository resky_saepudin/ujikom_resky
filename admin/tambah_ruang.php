<?php
include 'header.php';
?>
<?php
include 'cek_level.php';
?>
    <div id="wrapper">

      <!-- Sidebar -->
		<?php
		include 'menu.php';
		?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Ruang</h3>
          <hr>
			<center><h5>Tambah Ruang</h5></center>
			<hr>
<?php
include "koneksi.php";
$query = "SELECT max(kode_ruang) as maxKode FROM ruang";
$hasil = mysqli_query($koneksi,$query);
$data = mysqli_fetch_array($hasil);
$kodeBarang = $data['maxKode'];
$noUrut = (int) substr($kodeBarang, 4, 4);
$noUrut++;
$char = "RNG";
$kodeBarang = $char . sprintf("%04s", $noUrut);
?>
          <form action="proses_ruang.php?aksi=tambah" method="post">
			<div class="col-md-6">
				<div class="form-group">
				<label>Nama Ruang</label>
				<input name="nama_ruang" type="text" class="form-control" placeholder="Masukan Nama Ruang" autocomplete="off" maxlength="22" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Kode Ruang</label>
				<input name="kode_ruang" type="text" class="form-control" value="<?php echo $kodeBarang;?>" readonly="readonly" placeholder="Masukan Kode Ruang" autocomplete="off" maxlength="22" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Keterangan</label>
				<div class="form-label-group">
                <textarea name="keterangan" type="text" id="Keterangan" class="form-control" placeholder="Keterangan" autocomplete="off" maxlength="22" required=""></textarea>                
				</div>
            </div>
			</div>
             <div class="col-md-6">
								<button type="submit" class="btn btn-primary">Simpan</button>
								<a href="ruang.php"><button type="button" class="btn btn-">Kembali</button>
			</div>
			<br>
			<br>
          </form>
     
        
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <?php
	include 'script.php';
	?>

  </body>

</html>
