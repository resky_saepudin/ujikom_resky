<?php
include 'header.php';
?>
<?php
include 'cek_level.php';
?>
<?php
include "koneksi.php";
$id_inventaris=$_GET['id_inventaris'];

$select=mysqli_query($koneksi,"select * from inventaris where id_inventaris='$id_inventaris'");
$data=mysqli_fetch_array($select);
?>
    <div id="wrapper">

      <!-- Sidebar -->
      <?php
	  include 'menu.php';
	  ?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Inventaris</h3>
          <hr>
			<center><h5>Edit Inventaris</h5></center>
			<hr>

				<form action="update_inventaris.php" method="post"> 
				<div class="col-md-6">
				<div class="form-group">
				<label>Nama</label>
				<input name="id_inventaris" type="hidden" class="form-control" value="<?php echo $data['id_inventaris'];?>" placeholder="Masukan Username" autocomplete="off" maxlength="22" required="">
				<input name="nama" type="text" class="form-control" placeholder="Masukan Username" value="<?php echo $data['nama'];?>"autocomplete="off" maxlength="22" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Kondisi</label>
				<input name="kondisi" type="text" class="form-control" placeholder="Masukan Password" value="<?php echo $data['kondisi'];?>" autocomplete="off" maxlength="22" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Keterangan</label>
				<input name="keterangan_inv" type="text" class="form-control" placeholder="Masukan Nama Petugas" value="<?php echo $data['keterangan_inv'];?>" autocomplete="off" maxlength="" required="">
				</div>
			 </div>
			  <div class="col-md-6">
				<div class="form-group">
				<label>Jumlah</label>
				<input name="jumlah" type="number" class="form-control" placeholder="Masukan Nama Petugas" value="<?php echo $data['jumlah'];?>" autocomplete="off" maxlength="11" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Id Jenis</label>
				<select name="id_jenis" class="form-control m-bot15">
								<option><?php echo $data['id_jenis'];?></option>
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysqli_query($koneksi,"SELECT id_jenis,nama_jenis from jenis ");
								while($row = mysqli_fetch_assoc($result))
								{
								echo "<option>$row[id_jenis].$row[nama_jenis]</option>";
								} 
								?>
								
									</select>
				</div>
			 </div>
			  <div class="col-md-6">
				<div class="form-group">
				<label>Tanggal Register</label>
				<input name="tanggal_register" type="date" class="form-control" placeholder="Masukan Nama Petugas" value="<?php echo $data['tanggal_register'];?>" autocomplete="off" maxlength="" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Id Ruang</label>
				<select name="id_ruang" class="form-control m-bot15">
								<option><?php echo $data['id_ruang'];?></option>
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysqli_query($koneksi,"SELECT id_ruang,nama_ruang from ruang ");
								while($row = mysqli_fetch_assoc($result))
								{
								echo "<option>$row[id_ruang].$row[nama_ruang]</option>";
								} 
								?>
								
									</select>
				</div>
			 </div>
			  <div class="col-md-6">
				<div class="form-group">
				<label>Kode Inventaris</label>
				<input name="kode_inventaris" type="text" class="form-control" placeholder="Masukan Nama Petugas" value="<?php echo $data['kode_inventaris'];?>" autocomplete="off" readonly="readonly" maxlength="" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Id Petugas</label>
				<select name="id_petugas" class="form-control m-bot15">
								<option><?php echo $data['id_petugas'];?></option>
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysqli_query($koneksi,"SELECT id_petugas,nama_petugas from petugas ");
								while($row = mysqli_fetch_assoc($result))
								{
								echo "<option>$row[id_petugas].$row[nama_petugas]</option>";
								} 
								?>
								
									</select>
				</div>
			 </div>
			 <div class="col-md-6">
								<button type="submit" class="btn btn-primary">Update</button>
								<a href="inventaris.php"><button type="button" class="btn btn-">Kembali</button>
			</div>
			</form> 

          
        
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	
	 <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
	
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
	
	<script src="js/demo/datatables-demo.js"></script>

  </body>

</html>
