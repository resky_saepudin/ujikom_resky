<?php
include 'header.php';
?>
<?php
include 'cek_level.php';
?>
    <div id="wrapper">

		<?php
		include 'menu.php';
		?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Laporan Inventaris</h3>
          <hr>
		  <center><h4>Pilih Laporan Berdasarkan : </h4></center>
		  <br>
		  <div class="row">
			 <div class="col-xl-4 col-sm-6 mb-4">
              <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-file"></i>
                  </div>
                  <div class="mr-5">Laporan Per Tahun!</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="tahun_inventaris.php">
                  <span class="float-left">Lihat Laporan</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
			 <div class="col-xl-4 col-sm-6 mb-4">
              <div class="card text-white bg-warning o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-briefcase"></i>
                  </div>
                  <div class="mr-5">Laporan Per Bulan!</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="bulan_inventaris.php">
                  <span class="float-left">Lihat Laporan</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-4 col-sm-6 mb-4">
              <div class="card text-white bg-danger o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-calendar"></i>
                  </div>
                  <div class="mr-5">Laporan Per Tanggal!</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="tanggal_inventaris.php">
                  <span class="float-left">Lihat Laporan</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
			  </div>
            </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

<?php
include 'script.php';
?>

  </body>

</html>
